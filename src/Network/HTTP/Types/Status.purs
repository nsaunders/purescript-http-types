module Network.HTTP.Types.Status
( Status(..)
, mkStatus
, status100
, continue100
, status101
, switchingProtocols101
, status200
, ok200
, status201
, created201
, status202
, accepted202
, status203
, nonAuthoritative203
, status204
, noContent204
, status205
, resetContent205
, status206
, partialContent206
, status300
, multipleChoices300
, status301
, movedPermanently301
, status302
, found302
, status303
, seeOther303
, status304
, notModified304
, status305
, useProxy305
, status307
, temporaryRedirect307
, status308
, permanentRedirect308
, status400
, badRequest400
, status401
, unauthorized401
, status402
, paymentRequired402
, status403
, forbidden403
, status404
, notFound404
, status405
, methodNotAllowed405
, status406
, notAcceptable406
, status407
, proxyAuthenticationRequired407
, status408
, requestTimeout408
, status409
, conflict409
, status410
, gone410
, status411
, lengthRequired411
, status412
, preconditionFailed412
, status413
, requestEntityTooLarge413
, status414
, requestURITooLong414
, status415
, unsupportedMediaType415
, status416
, requestedRangeNotSatisfiable416
, status417
, expectationFailed417
, status418
, imATeapot418
, status422
, unprocessableEntity422
, status426
, upgradeRequired426
, status428
, preconditionRequired428
, status429
, tooManyRequests429
, status431
, requestHeaderFieldsTooLarge431
, status500
, internalServerError500
, status501
, notImplemented501
, status502
, badGateway502
, status503
, serviceUnavailable503
, status504
, gatewayTimeout504
, status505
, status511
, networkAuthenticationRequired511
, httpVersionNotSupported505
, statusIsInformational
, statusIsSuccessful
, statusIsRedirection
, statusIsClientError
, statusIsServerError
) where

import Prelude
import Data.Enum (class BoundedEnum, class Enum, Cardinality(..))
import Data.Maybe (Maybe(..))

-- | HTTP Status.
--
-- Only the 'statusCode' is used for comparisons.
--
-- Please use 'mkStatus' to create status codes from code and message, or the 'Enum' instance or the
-- status code constants (like 'ok200'). There might be additional record members in the future.
--
-- Note that the Show instance is only for debugging.
data Status
    = Status { statusCode :: Int
             , statusMessage :: String
             }
-- TODO: Typeable?
--       https://github.com/aristidb/http-types/blob/0.12.1/Network/HTTP/Types/Status.hs#L122

instance eqStatus :: Eq Status where
  eq (Status { statusCode: a }) (Status { statusCode: b }) = a == b

instance ordStatus :: Ord Status where
  compare (Status { statusCode: a }) (Status { statusCode: b }) = compare a b

instance enumStatus :: Enum Status where
  succ (Status { statusCode: 100 }) = Just status101
  succ (Status { statusCode: 101 }) = Just status200
  succ (Status { statusCode: 200 }) = Just status201
  succ (Status { statusCode: 201 }) = Just status202
  succ (Status { statusCode: 202 }) = Just status203
  succ (Status { statusCode: 203 }) = Just status204
  succ (Status { statusCode: 204 }) = Just status205
  succ (Status { statusCode: 205 }) = Just status206
  succ (Status { statusCode: 206 }) = Just status300
  succ (Status { statusCode: 300 }) = Just status301
  succ (Status { statusCode: 301 }) = Just status302
  succ (Status { statusCode: 302 }) = Just status303
  succ (Status { statusCode: 303 }) = Just status304
  succ (Status { statusCode: 304 }) = Just status305
  succ (Status { statusCode: 305 }) = Just status307
  succ (Status { statusCode: 307 }) = Just status308
  succ (Status { statusCode: 308 }) = Just status400
  succ (Status { statusCode: 400 }) = Just status401
  succ (Status { statusCode: 401 }) = Just status402
  succ (Status { statusCode: 402 }) = Just status403
  succ (Status { statusCode: 403 }) = Just status404
  succ (Status { statusCode: 404 }) = Just status405
  succ (Status { statusCode: 405 }) = Just status406
  succ (Status { statusCode: 406 }) = Just status407
  succ (Status { statusCode: 407 }) = Just status408
  succ (Status { statusCode: 408 }) = Just status409
  succ (Status { statusCode: 409 }) = Just status410
  succ (Status { statusCode: 410 }) = Just status411
  succ (Status { statusCode: 411 }) = Just status412
  succ (Status { statusCode: 412 }) = Just status413
  succ (Status { statusCode: 413 }) = Just status414
  succ (Status { statusCode: 414 }) = Just status415
  succ (Status { statusCode: 415 }) = Just status416
  succ (Status { statusCode: 416 }) = Just status417
  succ (Status { statusCode: 417 }) = Just status418
  succ (Status { statusCode: 418 }) = Just status422
  succ (Status { statusCode: 422 }) = Just status426
  succ (Status { statusCode: 426 }) = Just status428
  succ (Status { statusCode: 428 }) = Just status429
  succ (Status { statusCode: 429 }) = Just status431
  succ (Status { statusCode: 431 }) = Just status500
  succ (Status { statusCode: 500 }) = Just status501
  succ (Status { statusCode: 501 }) = Just status502
  succ (Status { statusCode: 502 }) = Just status503
  succ (Status { statusCode: 503 }) = Just status504
  succ (Status { statusCode: 504 }) = Just status505
  succ (Status { statusCode: 505 }) = Just status511
  succ (Status { statusCode: 511 }) = Nothing
  succ (Status { statusCode: _   }) = Nothing
  pred (Status { statusCode: 100 }) = Nothing
  pred (Status { statusCode: 101 }) = Just status100
  pred (Status { statusCode: 200 }) = Just status101
  pred (Status { statusCode: 201 }) = Just status200
  pred (Status { statusCode: 202 }) = Just status201
  pred (Status { statusCode: 203 }) = Just status202
  pred (Status { statusCode: 204 }) = Just status203
  pred (Status { statusCode: 205 }) = Just status204
  pred (Status { statusCode: 206 }) = Just status205
  pred (Status { statusCode: 300 }) = Just status206
  pred (Status { statusCode: 301 }) = Just status300
  pred (Status { statusCode: 302 }) = Just status301
  pred (Status { statusCode: 303 }) = Just status302
  pred (Status { statusCode: 304 }) = Just status303
  pred (Status { statusCode: 305 }) = Just status304
  pred (Status { statusCode: 307 }) = Just status305
  pred (Status { statusCode: 308 }) = Just status307
  pred (Status { statusCode: 400 }) = Just status308
  pred (Status { statusCode: 401 }) = Just status400
  pred (Status { statusCode: 402 }) = Just status401
  pred (Status { statusCode: 403 }) = Just status402
  pred (Status { statusCode: 404 }) = Just status403
  pred (Status { statusCode: 405 }) = Just status404
  pred (Status { statusCode: 406 }) = Just status405
  pred (Status { statusCode: 407 }) = Just status406
  pred (Status { statusCode: 408 }) = Just status407
  pred (Status { statusCode: 409 }) = Just status408
  pred (Status { statusCode: 410 }) = Just status409
  pred (Status { statusCode: 411 }) = Just status410
  pred (Status { statusCode: 412 }) = Just status411
  pred (Status { statusCode: 413 }) = Just status412
  pred (Status { statusCode: 414 }) = Just status413
  pred (Status { statusCode: 415 }) = Just status414
  pred (Status { statusCode: 416 }) = Just status415
  pred (Status { statusCode: 417 }) = Just status416
  pred (Status { statusCode: 418 }) = Just status417
  pred (Status { statusCode: 422 }) = Just status418
  pred (Status { statusCode: 426 }) = Just status422
  pred (Status { statusCode: 428 }) = Just status426
  pred (Status { statusCode: 429 }) = Just status428
  pred (Status { statusCode: 431 }) = Just status429
  pred (Status { statusCode: 500 }) = Just status431
  pred (Status { statusCode: 501 }) = Just status500
  pred (Status { statusCode: 502 }) = Just status501
  pred (Status { statusCode: 503 }) = Just status502
  pred (Status { statusCode: 504 }) = Just status503
  pred (Status { statusCode: 505 }) = Just status504
  pred (Status { statusCode: 511 }) = Just status505
  pred (Status { statusCode: _   }) = Nothing

instance boundedStatus :: Bounded Status where
  bottom = status100
  top = status511

instance boundedEnumStatus :: BoundedEnum Status where
  cardinality = Cardinality 48
  fromEnum (Status { statusCode }) = statusCode
  toEnum 100 = Just status100
  toEnum 101 = Just status101
  toEnum 200 = Just status200
  toEnum 201 = Just status201
  toEnum 202 = Just status202
  toEnum 203 = Just status203
  toEnum 204 = Just status204
  toEnum 205 = Just status205
  toEnum 206 = Just status206
  toEnum 300 = Just status300
  toEnum 301 = Just status301
  toEnum 302 = Just status302
  toEnum 303 = Just status303
  toEnum 304 = Just status304
  toEnum 305 = Just status305
  toEnum 307 = Just status307
  toEnum 308 = Just status308
  toEnum 400 = Just status400
  toEnum 401 = Just status401
  toEnum 402 = Just status402
  toEnum 403 = Just status403
  toEnum 404 = Just status404
  toEnum 405 = Just status405
  toEnum 406 = Just status406
  toEnum 407 = Just status407
  toEnum 408 = Just status408
  toEnum 409 = Just status409
  toEnum 410 = Just status410
  toEnum 411 = Just status411
  toEnum 412 = Just status412
  toEnum 413 = Just status413
  toEnum 414 = Just status414
  toEnum 415 = Just status415
  toEnum 416 = Just status416
  toEnum 417 = Just status417
  toEnum 418 = Just status418
  toEnum 422 = Just status422
  toEnum 426 = Just status426
  toEnum 428 = Just status428
  toEnum 429 = Just status429
  toEnum 431 = Just status431
  toEnum 500 = Just status500
  toEnum 501 = Just status501
  toEnum 502 = Just status502
  toEnum 503 = Just status503
  toEnum 504 = Just status504
  toEnum 505 = Just status505
  toEnum 511 = Just status511
  toEnum _   = Nothing

-- | Create a Status from status code and message.
mkStatus :: Int -> String -> Status
mkStatus statusCode statusMessage = Status { statusCode, statusMessage }

-- | Continue 100
status100 :: Status
status100 = mkStatus 100 "Continue"

-- | Continue 100
continue100 :: Status
continue100 = status100

-- | Switching Protocols 101
status101 :: Status
status101 = mkStatus 101 "Switching Protocols"

-- | Switching Protocols 101
switchingProtocols101 :: Status
switchingProtocols101 = status101

-- | OK 200
status200 :: Status
status200 = mkStatus 200 "OK"

-- | OK 200
ok200 :: Status
ok200 = status200

-- | Created 201
status201 :: Status
status201 = mkStatus 201 "Created"

-- | Created 201
created201 :: Status
created201 = status201

-- | Accepted 202
status202 :: Status
status202 = mkStatus 202 "Accepted"

-- | Accepted 202
accepted202 :: Status
accepted202 = status202

-- | Non-Authoritative Information 203
status203 :: Status
status203 = mkStatus 203 "Non-Authoritative Information"

-- | Non-Authoritative Information 203
nonAuthoritative203 :: Status
nonAuthoritative203 = status203

-- | No Content 204
status204 :: Status
status204 = mkStatus 204 "No Content"

-- | No Content 204
noContent204 :: Status
noContent204 = status204

-- | Reset Content 205
status205 :: Status
status205 = mkStatus 205 "Reset Content"

-- | Reset Content 205
resetContent205 :: Status
resetContent205 = status205

-- | Partial Content 206
status206 :: Status
status206 = mkStatus 206 "Partial Content"

-- | Partial Content 206
partialContent206 :: Status
partialContent206 = status206

-- | Multiple Choices 300
status300 :: Status
status300 = mkStatus 300 "Multiple Choices"

-- | Multiple Choices 300
multipleChoices300 :: Status
multipleChoices300 = status300

-- | Moved Permanently 301
status301 :: Status
status301 = mkStatus 301 "Moved Permanently"

-- | Moved Permanently 301
movedPermanently301 :: Status
movedPermanently301 = status301

-- | Found 302
status302 :: Status
status302 = mkStatus 302 "Found"

-- | Found 302
found302 :: Status
found302 = status302

-- | See Other 303
status303 :: Status
status303 = mkStatus 303 "See Other"

-- | See Other 303
seeOther303 :: Status
seeOther303 = status303

-- | Not Modified 304
status304 :: Status
status304 = mkStatus 304 "Not Modified"

-- | Not Modified 304
notModified304 :: Status
notModified304 = status304

-- | Use Proxy 305
status305 :: Status
status305 = mkStatus 305 "Use Proxy"

-- | Use Proxy 305
useProxy305 :: Status
useProxy305 = status305

-- | Temporary Redirect 307
status307 :: Status
status307 = mkStatus 307 "Temporary Redirect"

-- | Temporary Redirect 307
temporaryRedirect307 :: Status
temporaryRedirect307 = status307

-- | Permanent Redirect 308
status308 :: Status
status308 = mkStatus 308 "Permanent Redirect"

-- | Permanent Redirect 308
permanentRedirect308 :: Status
permanentRedirect308 = status308

-- | Bad Request 400
status400 :: Status
status400 = mkStatus 400 "Bad Request"

-- | Bad Request 400
badRequest400 :: Status
badRequest400 = status400

-- | Unauthorized 401
status401 :: Status
status401 = mkStatus 401 "Unauthorized"

-- | Unauthorized 401
unauthorized401 :: Status
unauthorized401 = status401

-- | Payment Required 402
status402 :: Status
status402 = mkStatus 402 "Payment Required"

-- | Payment Required 402
paymentRequired402 :: Status
paymentRequired402 = status402

-- | Forbidden 403
status403 :: Status
status403 = mkStatus 403 "Forbidden"

-- | Forbidden 403
forbidden403 :: Status
forbidden403 = status403

-- | Not Found 404
status404 :: Status
status404 = mkStatus 404 "Not Found"

-- | Not Found 404
notFound404 :: Status
notFound404 = status404

-- | Method Not Allowed 405
status405 :: Status
status405 = mkStatus 405 "Method Not Allowed"

-- | Method Not Allowed 405
methodNotAllowed405 :: Status
methodNotAllowed405 = status405

-- | Not Acceptable 406
status406 :: Status
status406 = mkStatus 406 "Not Acceptable"

-- | Not Acceptable 406
notAcceptable406 :: Status
notAcceptable406 = status406

-- | Proxy Authentication Required 407
status407 :: Status
status407 = mkStatus 407 "Proxy Authentication Required"

-- | Proxy Authentication Required 407
proxyAuthenticationRequired407 :: Status
proxyAuthenticationRequired407 = status407

-- | Request Timeout 408
status408 :: Status
status408 = mkStatus 408 "Request Timeout"

-- | Request Timeout 408
requestTimeout408 :: Status
requestTimeout408 = status408

-- | Conflict 409
status409 :: Status
status409 = mkStatus 409 "Conflict"

-- | Conflict 409
conflict409 :: Status
conflict409 = status409

-- | Gone 410
status410 :: Status
status410 = mkStatus 410 "Gone"

-- | Gone 410
gone410 :: Status
gone410 = status410

-- | Length Required 411
status411 :: Status
status411 = mkStatus 411 "Length Required"

-- | Length Required 411
lengthRequired411 :: Status
lengthRequired411 = status411

-- | Precondition Failed 412
status412 :: Status
status412 = mkStatus 412 "Precondition Failed"

-- | Precondition Failed 412
preconditionFailed412 :: Status
preconditionFailed412 = status412

-- | Request Entity Too Large 413
status413 :: Status
status413 = mkStatus 413 "Request Entity Too Large"

-- | Request Entity Too Large 413
requestEntityTooLarge413 :: Status
requestEntityTooLarge413 = status413

-- | Request-URI Too Long 414
status414 :: Status
status414 = mkStatus 414 "Request-URI Too Long"

-- | Request-URI Too Long 414
requestURITooLong414 :: Status
requestURITooLong414 = status414

-- | Unsupported Media Type 415
status415 :: Status
status415 = mkStatus 415 "Unsupported Media Type"

-- | Unsupported Media Type 415
unsupportedMediaType415 :: Status
unsupportedMediaType415 = status415

-- | Requested Range Not Satisfiable 416
status416 :: Status
status416 = mkStatus 416 "Requested Range Not Satisfiable"

-- | Requested Range Not Satisfiable 416
requestedRangeNotSatisfiable416 :: Status
requestedRangeNotSatisfiable416 = status416

-- | Expectation Failed 417
status417 :: Status
status417 = mkStatus 417 "Expectation Failed"

-- | Expectation Failed 417
expectationFailed417 :: Status
expectationFailed417 = status417

-- | I'm a teapot 418
status418 :: Status
status418 = mkStatus 418 "I'm a teapot"

-- | I'm a teapot 418
imATeapot418 :: Status
imATeapot418 = status418

-- | Unprocessable Entity 422
-- (<https://tools.ietf.org/html/rfc4918 RFC 4918>)
status422 :: Status
status422 = mkStatus 422 "Unprocessable Entity"

-- | Unprocessable Entity 422
-- (<https://tools.ietf.org/html/rfc4918 RFC 4918>)
unprocessableEntity422 :: Status
unprocessableEntity422 = status422

-- | Upgrade Required 426
-- (<https://tools.ietf.org/html/rfc7231#section-6.5.15>)
status426 :: Status
status426 = mkStatus 426 "Upgrade Required"

-- | Upgrade Required 426
-- (<https://tools.ietf.org/html/rfc7231#section-6.5.15>)
upgradeRequired426 :: Status
upgradeRequired426 = status426

-- | Precondition Required 428
-- (<https://tools.ietf.org/html/rfc6585 RFC 6585>)
status428 :: Status
status428 = mkStatus 428 "Precondition Required"

-- | Precondition Required 428
-- (<https://tools.ietf.org/html/rfc6585 RFC 6585>)
preconditionRequired428 :: Status
preconditionRequired428 = status428

-- | Too Many Requests 429
-- (<https://tools.ietf.org/html/rfc6585 RFC 6585>)
status429 :: Status
status429 = mkStatus 429 "Too Many Requests"

-- | Too Many Requests 429
-- (<https://tools.ietf.org/html/rfc6585 RFC 6585>)
tooManyRequests429 :: Status
tooManyRequests429 = status429

-- | Request Header Fields Too Large 431
-- (<https://tools.ietf.org/html/rfc6585 RFC 6585>)
status431 :: Status
status431 = mkStatus 431 "Request Header Fields Too Large"

-- | Request Header Fields Too Large 431
-- (<https://tools.ietf.org/html/rfc6585 RFC 6585>)
requestHeaderFieldsTooLarge431 :: Status
requestHeaderFieldsTooLarge431 = status431

-- | Internal Server Error 500
status500 :: Status
status500 = mkStatus 500 "Internal Server Error"

-- | Internal Server Error 500
internalServerError500 :: Status
internalServerError500 = status500

-- | Not Implemented 501
status501 :: Status
status501 = mkStatus 501 "Not Implemented"

-- | Not Implemented 501
notImplemented501 :: Status
notImplemented501 = status501

-- | Bad Gateway 502
status502 :: Status
status502 = mkStatus 502 "Bad Gateway"

-- | Bad Gateway 502
badGateway502 :: Status
badGateway502 = status502

-- | Service Unavailable 503
status503 :: Status
status503 = mkStatus 503 "Service Unavailable"

-- | Service Unavailable 503
serviceUnavailable503 :: Status
serviceUnavailable503 = status503

-- | Gateway Timeout 504
status504 :: Status
status504 = mkStatus 504 "Gateway Timeout"

-- | Gateway Timeout 504
gatewayTimeout504 :: Status
gatewayTimeout504 = status504

-- | HTTP Version Not Supported 505
status505 :: Status
status505 = mkStatus 505 "HTTP Version Not Supported"

-- | HTTP Version Not Supported 505
httpVersionNotSupported505 :: Status
httpVersionNotSupported505 = status505

-- | Network Authentication Required 511
-- (<https://tools.ietf.org/html/rfc6585 RFC 6585>)
status511 :: Status
status511 = mkStatus 511 "Network Authentication Required"

-- | Network Authentication Required 511
-- (<https://tools.ietf.org/html/rfc6585 RFC 6585>)
networkAuthenticationRequired511 :: Status
networkAuthenticationRequired511 = status511

-- | Informational class
statusIsInformational :: Status -> Boolean
statusIsInformational (Status { statusCode: code }) = code >= 100 && code < 200

-- | Successful class
statusIsSuccessful :: Status -> Boolean
statusIsSuccessful (Status { statusCode: code }) = code >= 200 && code < 300

-- | Redirection class
statusIsRedirection :: Status -> Boolean
statusIsRedirection (Status { statusCode: code }) = code >= 300 && code < 400

-- | Client Error class
statusIsClientError :: Status -> Boolean
statusIsClientError (Status { statusCode: code }) = code >= 400 && code < 500

-- | Server Error class
statusIsServerError :: Status -> Boolean
statusIsServerError (Status { statusCode: code }) = code >= 500 && code < 600
