module Network.HTTP.Types.Header
(
  -- ** Types
  Header
, HeaderName
, RequestHeaders
, ResponseHeaders
  -- ** Common headers
, hAccept
, hAcceptCharset
, hAcceptEncoding
, hAcceptLanguage
, hAcceptRanges
, hAge
, hAllow
, hAuthorization
, hCacheControl
, hConnection
, hContentEncoding
, hContentLanguage
, hContentLength
, hContentLocation
, hContentMD5
, hContentRange
, hContentType
, hDate
, hETag
, hExpect
, hExpires
, hFrom
, hHost
, hIfMatch
, hIfModifiedSince
, hIfNoneMatch
, hIfRange
, hIfUnmodifiedSince
, hLastModified
, hLocation
, hMaxForwards
, hOrigin
, hPragma
, hProxyAuthenticate
, hProxyAuthorization
, hRange
, hReferer
, hRetryAfter
, hServer
, hTE
, hTrailer
, hTransferEncoding
, hUpgrade
, hUserAgent
, hVary
, hVia
, hWWWAuthenticate
, hWarning
, hContentDisposition
, hMIMEVersion
, hCookie
, hSetCookie
)
where

import Data.String.CaseInsensitive (CaseInsensitiveString(..))
import Data.Tuple (Tuple)

-- | Header
type Header = Tuple HeaderName String

-- | Header name
type HeaderName = CaseInsensitiveString

-- | Request Headers
type RequestHeaders = Array Header

-- | Response Headers
type ResponseHeaders = Array Header

h :: String -> HeaderName
h = CaseInsensitiveString

-- | HTTP Header names according to http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
hAccept             = h "Accept"              :: HeaderName
hAcceptCharset      = h "Accept-Charset"      :: HeaderName
hAcceptEncoding     = h "Accept-Encoding"     :: HeaderName
hAcceptLanguage     = h "Accept-Language"     :: HeaderName
hAcceptRanges       = h "Accept-Ranges"       :: HeaderName
hAge                = h "Age"                 :: HeaderName
hAllow              = h "Allow"               :: HeaderName
hAuthorization      = h "Authorization"       :: HeaderName
hCacheControl       = h "Cache-Control"       :: HeaderName
hConnection         = h "Connection"          :: HeaderName
hContentEncoding    = h "Content-Encoding"    :: HeaderName
hContentLanguage    = h "Content-Language"    :: HeaderName
hContentLength      = h "Content-Length"      :: HeaderName
hContentLocation    = h "Content-Location"    :: HeaderName
hContentMD5         = h "Content-MD5"         :: HeaderName
hContentRange       = h "Content-Range"       :: HeaderName
hContentType        = h "Content-Type"        :: HeaderName
hDate               = h "Date"                :: HeaderName
hETag               = h "ETag"                :: HeaderName
hExpect             = h "Expect"              :: HeaderName
hExpires            = h "Expires"             :: HeaderName
hFrom               = h "From"                :: HeaderName
hHost               = h "Host"                :: HeaderName
hIfMatch            = h "If-Match"            :: HeaderName
hIfModifiedSince    = h "If-Modified-Since"   :: HeaderName
hIfNoneMatch        = h "If-None-Match"       :: HeaderName
hIfRange            = h "If-Range"            :: HeaderName
hIfUnmodifiedSince  = h "If-Unmodified-Since" :: HeaderName
hLastModified       = h "Last-Modified"       :: HeaderName
hLocation           = h "Location"            :: HeaderName
hMaxForwards        = h "Max-Forwards"        :: HeaderName
hPragma             = h "Pragma"              :: HeaderName
hProxyAuthenticate  = h "Proxy-Authenticate"  :: HeaderName
hProxyAuthorization = h "Proxy-Authorization" :: HeaderName
hRange              = h "Range"               :: HeaderName
hReferer            = h "Referer"             :: HeaderName
hRetryAfter         = h "Retry-After"         :: HeaderName
hServer             = h "Server"              :: HeaderName
hTE                 = h "TE"                  :: HeaderName
hTrailer            = h "Trailer"             :: HeaderName
hTransferEncoding   = h "Transfer-Encoding"   :: HeaderName
hUpgrade            = h "Upgrade"             :: HeaderName
hUserAgent          = h "User-Agent"          :: HeaderName
hVary               = h "Vary"                :: HeaderName
hVia                = h "Via"                 :: HeaderName
hWWWAuthenticate    = h "WWW-Authenticate"    :: HeaderName
hWarning            = h "Warning"             :: HeaderName

-- | HTTP Header names according to http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html
hContentDisposition = h "Content-Disposition" :: HeaderName
hMIMEVersion        = h "MIME-Version"        :: HeaderName

-- | HTTP Header names according to https://tools.ietf.org/html/rfc6265#section-4
hCookie             = h "Cookie"              :: HeaderName
hSetCookie          = h "Set-Cookie"          :: HeaderName

-- | HTTP Header names according to https://tools.ietf.org/html/rfc6454
hOrigin = h "Origin"                          :: HeaderName

-- TODO: Byte range stuff - not sure if really important
--       https://github.com/aristidb/http-types/blob/0.12.1/Network/HTTP/Types/Header.hs#L162
