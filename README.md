Generic HTTP types for PureScript
=================================

This project seeks to port [http-types](https://github.com/aristidb/http-types) to PureScript as faithfully as possible
in the hopes that dependent Haskell libraries might be ported over to PureScript just a bit more easily. Currently, it is
a subset of that library, but please feel free to submit a pull request.
